// login
export const API_ENDPOINT_LOGIN_FIREBASE = 'login/firebase';

// user
export const API_ENDPOINT_GET_USER = 'users';
export const API_ENDPOINT_REGISTER = 'user/register';

// activity
export const API_ENDPOINT_GET_ACTIVITY = 'report/activity';
export const API_ENDPOINT_BILL_DETAIL = 'bills/';
